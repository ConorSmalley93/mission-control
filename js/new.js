var refreshRate = 1000;
$(document).ready(function() {


	$('.dropmenu').click(function(e) {
		e.preventDefault();
		$(this).parent().find('ul').slideToggle();
	});


	Temperature();
	Sound();
	Light();
	All();
	sparkline_charts();

});
/*These functions load data from the csv using AJAX and calls the coresponding chard functions.*/
function Temperature() {
	$.ajax({
		// url: 'data/data.csv',
		url: 'data/allData.csv',
		dataType: 'text',
	}).done(loadTempChart);

	setInterval(function() {

		$.ajax({
			// url: 'data/data.csv',
			url: 'data/allData.csv',
			dataType: 'text',
		}).done(loadTempChart);
	}, refreshRate);
}

function Sound() {
	$.ajax({
		// url: 'data/data.csv',
		url: 'data/allData.csv',
		dataType: 'text',
	}).done(loadSoundChart);

	setInterval(function() {

		$.ajax({
			// url: 'data/data.csv',
			url: 'data/allData.csv',
			dataType: 'text',
		}).done(loadSoundChart);
	}, refreshRate);
}

function Light() {
	$.ajax({
		// url: 'data/data.csv',
		url: 'data/allData.csv',
		dataType: 'text',
	}).done(loadLightChart);

	setInterval(function() {

		$.ajax({
			// url: 'data/data.csv',
			url: 'data/allData.csv',
			dataType: 'text',
		}).done(loadLightChart);
	}, refreshRate);
}

function All() {
	$.ajax({
		// url: 'data/data.csv',
		url: 'data/allData.csv',
		dataType: 'text',
	}).done(loadAllChart);

	setInterval(function() {

		$.ajax({
			// url: 'data/data.csv',
			url: 'data/allData.csv',
			dataType: 'text',
		}).done(loadAllChart);
	}, refreshRate);
}
/*These function load the charts.*/
function loadTempChart(data) {
	/* ---------- Chart with points ---------- */
	if ($("#liveTemperature").length) {

		var tempData = [];
		var allRows = data.split(/\r?\n|\r/);
		var average = 0;
		for (var singleRow = 0; singleRow < allRows.length; singleRow++) {
			if (allRows[singleRow] === "") {

			} else {
				// console.log(allRows[singleRow]);
				var rowCells = allRows[singleRow].split(',');
				for (var rowCell = 0; rowCell < rowCells.length; rowCell++) {
					// tempData.push(rowCells);
					tempData.push([rowCells[0], rowCells[1]]);
					// console.log(allRows.length);
					if (rowCell === 1) {
						// console.log(rowCells[rowCell]);
						average = average + parseInt(rowCells[rowCell]);
					}
				}
			}


		}
		average = (average / (allRows.length - 1)).toFixed(2);


		try {
			document.getElementById("AverageTemp").innerHTML = average + "&deg;C";
		} catch (err) {
			// document.getElementById("demo").innerHTML = err.message;
		}


		var plot = $.plot($("#liveTemperature"), [{
			data: tempData,
			label: "Temperature",
			lines: {
				show: true,
				fill: false,
				lineWidth: 2
			},
			points: { show: $("#tempPointsCheckbox").is(':checked') },
			shadowSize: 0
		}], {

			grid: {
				hoverable: true,
				clickable: true,
				tickColor: "rgba(255,255,255,0.05)",
				borderWidth: 0
			},
			legend: {
				show: false
			},
			colors: ["rgba(255,255,255,0.8)", "rgba(255,255,255,0.6)", "rgba(255,255,255,0.4)", "rgba(255,255,255,0.2)"],
			xaxis: {
				// ticks: 15,
				mode: "time",
				timeformat: "%H:%M",
				tickDecimals: 0,
				color: "rgba(255,255,255,0.8)"
			},
			yaxis: {
				// ticks: 5,
				tickDecimals: 0,
				tickFormatter: function(v) {
					return v + "&deg;C";
				},
				color: "rgba(255,255,255,0.8)"
			},
		});



		function showTooltip(x, y, contents) {
			$('<div id="tooltip">' + contents + '</div>').css({
				position: 'absolute',
				display: 'none',
				top: y + 5,
				left: x - 75,
				border: '1px solid #fdd',
				padding: '2px',
				'background-color': '#dfeffc',
				opacity: 0.80
			}).appendTo("body").fadeIn(200);
		}

		var previousPoint = null;
		$("#liveTemperature").bind("plothover", function(event, pos, item) {
			$("#x").text(pos.x.toFixed(2));
			$("#y").text(pos.y.toFixed(2));

			if (item) {
				if (previousPoint != item.dataIndex) {
					previousPoint = item.dataIndex;

					$("#tooltip").remove();
					var x = item.datapoint[0].toFixed(2),
						y = item.datapoint[1].toFixed(2);

					showTooltip(item.pageX, item.pageY,
						item.series.label + " at " + ("0" + new Date(x * 1).getHours()).substr(-2) + ":" + ("0" + new Date(x * 1).getMinutes()).substr(-2) + ":" + ("0" + new Date(x * 1).getSeconds()).substr(-2) + " is " + y + "&deg;C");
				}
			} else {
				$("#tooltip").remove();
				previousPoint = null;
			}
		});

	}


}

function loadSoundChart(data) {

	/* ---------- Chart with points ---------- */
	if ($("#liveSound").length) {

		var soundData = [];
		var allRows = data.split(/\r?\n|\r/);
		var average = 0;
		for (var singleRow = 0; singleRow < allRows.length; singleRow++) {
			if (allRows[singleRow] === "") {

			} else {
				console.log(allRows[singleRow]);
				var rowCells = allRows[singleRow].split(',');
				for (var rowCell = 0; rowCell < rowCells.length; rowCell++) {
					// soundData.push(rowCells);
					soundData.push([rowCells[0], rowCells[2]]);
					// console.log(allRows.length);
					if (rowCell === 2) {
						// console.log(rowCells[rowCell]);
						average = average + parseInt(rowCells[rowCell]);
					}
				}
			}


		}
		average = (average / (allRows.length - 1)).toFixed(2);
		try {
			document.getElementById("AverageSound").innerHTML = average + "dB";
		} catch (err) {
			// document.getElementById("demo").innerHTML = err.message;
		}

		var plot = $.plot($("#liveSound"), [{
			data: soundData,
			label: "Volume",
			lines: {
				show: true,
				fill: false,
				lineWidth: 2
			},
			points: { show: $("#soundPointsCheckbox").is(':checked') },
			shadowSize: 0
		}], {

			grid: {
				hoverable: true,
				clickable: true,
				tickColor: "rgba(255,255,255,0.05)",
				borderWidth: 0
			},
			legend: {
				show: false
			},
			colors: ["rgba(255,255,255,0.8)", "rgba(255,255,255,0.6)", "rgba(255,255,255,0.4)", "rgba(255,255,255,0.2)"],
			xaxis: {
				// ticks: 15,
				mode: "time",
				timeformat: "%H:%M",
				tickDecimals: 0,
				color: "rgba(255,255,255,0.8)"
			},
			yaxis: {
				// ticks: 5,
				tickDecimals: 0,
				tickFormatter: function(v) {
					return v + "dB";
				},
				color: "rgba(255,255,255,0.8)"
			},
		});



		function showTooltip(x, y, contents) {
			$('<div id="tooltip">' + contents + '</div>').css({
				position: 'absolute',
				display: 'none',
				top: y + 5,
				left: x - 75,
				border: '1px solid #fdd',
				padding: '2px',
				'background-color': '#dfeffc',
				opacity: 0.80
			}).appendTo("body").fadeIn(200);
		}

		var previousPoint = null;
		$("#liveSound").bind("plothover", function(event, pos, item) {
			$("#x").text(pos.x.toFixed(2));
			$("#y").text(pos.y.toFixed(2));

			if (item) {
				if (previousPoint != item.dataIndex) {
					previousPoint = item.dataIndex;

					$("#tooltip").remove();
					var x = item.datapoint[0].toFixed(2),
						y = item.datapoint[1].toFixed(2);

					showTooltip(item.pageX, item.pageY,
						item.series.label + " at " + ("0" + new Date(x * 1).getHours()).substr(-2) + ":" + ("0" + new Date(x * 1).getMinutes()).substr(-2) + ":" + ("0" + new Date(x * 1).getSeconds()).substr(-2) + " is " + y + "dB");
				}
			} else {
				$("#tooltip").remove();
				previousPoint = null;
			}
		});

	}


}

function loadLightChart(data) {

	/* ---------- Chart with points ---------- */
	if ($("#liveLight").length) {

		var lightData = [];
		var allRows = data.split(/\r?\n|\r/);
		var average = 0;
		for (var singleRow = 0; singleRow < allRows.length; singleRow++) {
			if (allRows[singleRow] === "") {

			} else {
				console.log(allRows[singleRow]);
				var rowCells = allRows[singleRow].split(',');
				for (var rowCell = 0; rowCell < rowCells.length; rowCell++) {
					// lightData.push(rowCells);
					lightData.push([rowCells[0], rowCells[3]]);
					// console.log(allRows.length);
					if (rowCell === 3) {
						// console.log(rowCells[rowCell]);
						average = average + parseInt(rowCells[rowCell]);
					}
				}
			}


		}
		average = (average / (allRows.length - 1)).toFixed(2);
		try {
			document.getElementById("AverageLight").innerHTML = average + "lx";
		} catch (err) {
			// document.getElementById("demo").innerHTML = err.message;
		}

		var plot = $.plot($("#liveLight"), [{
			data: lightData,
			label: "Light Intensity",
			lines: {
				show: true,
				fill: false,
				lineWidth: 2
			},
			points: { show: $("#lightPointsCheckbox").is(':checked') },
			shadowSize: 0
		}], {

			grid: {
				hoverable: true,
				clickable: true,
				tickColor: "rgba(255,255,255,0.05)",
				borderWidth: 0
			},
			legend: {
				show: false
			},
			colors: ["rgba(255,255,255,0.8)", "rgba(255,255,255,0.6)", "rgba(255,255,255,0.4)", "rgba(255,255,255,0.2)"],
			xaxis: {
				// ticks: 15,
				mode: "time",
				timeformat: "%H:%M",
				tickDecimals: 0,
				color: "rgba(255,255,255,0.8)"
			},
			yaxis: {
				// ticks: 5,
				tickDecimals: 0,
				tickFormatter: function(v) {
					return v + "lx";
				},
				color: "rgba(255,255,255,0.8)"
			},
		});



		function showTooltip(x, y, contents) {
			$('<div id="tooltip">' + contents + '</div>').css({
				position: 'absolute',
				display: 'none',
				top: y + 5,
				left: x - 75,
				border: '1px solid #fdd',
				padding: '2px',
				'background-color': '#dfeffc',
				opacity: 0.80
			}).appendTo("body").fadeIn(200);
		}

		var previousPoint = null;
		$("#liveLight").bind("plothover", function(event, pos, item) {
			$("#x").text(pos.x.toFixed(2));
			$("#y").text(pos.y.toFixed(2));

			if (item) {
				if (previousPoint != item.dataIndex) {
					previousPoint = item.dataIndex;

					$("#tooltip").remove();
					var x = item.datapoint[0].toFixed(2),
						y = item.datapoint[1].toFixed(2);

					showTooltip(item.pageX, item.pageY,
						item.series.label + " at " + ("0" + new Date(x * 1).getHours()).substr(-2) + ":" + ("0" + new Date(x * 1).getMinutes()).substr(-2) + ":" + ("0" + new Date(x * 1).getSeconds()).substr(-2) + " is " + y + "lx");
				}
			} else {
				$("#tooltip").remove();
				previousPoint = null;
			}
		});

	}


}

function loadAllChart(data) {

	/* ---------- Chart with points ---------- */
	if ($("#liveAll").length) {

		var tempData = [];
		var lightData = [];
		var soundData = [];
		var feedStartData = [];
		var feedFinishData = [];
		var allRows = data.split(/\r?\n|\r/);
		var average = 0;
		for (var singleRow = 0; singleRow < allRows.length; singleRow++) {
			if (allRows[singleRow] === "") {

			} else {
				console.log(allRows[singleRow]);
				var rowCells = allRows[singleRow].split(',');
				for (var rowCell = 0; rowCell < rowCells.length; rowCell++) {
					// tempData.push(rowCells);
					tempData.push([rowCells[0], rowCells[1]]);
					soundData.push([rowCells[0], rowCells[2]]);
					lightData.push([rowCells[0], rowCells[3]]);
					feedStartData.push([rowCells[0], rowCells[4]]);
					feedFinishData.push([rowCells[0], rowCells[5]]);
				}
			}


		}

		var plot = $.plot($("#liveAll"), [{
			data: tempData,
			label: "Room Temperature",
			lines: {
				show: true,
				fill: false,
				lineWidth: 2
			},
			points: { show: $("#tempPointsCheckbox").is(':checked') },
			shadowSize: 0
		}, {
			data: soundData,
			label: "Room Volume",
			lines: {
				show: true,
				fill: false,
				lineWidth: 2
			},
			points: { show: $("#soundPointsCheckbox").is(':checked') },
			shadowSize: 0
		}, {
			data: lightData,
			label: "Room Light",
			lines: {
				show: true,
				fill: false,
				lineWidth: 2
			},
			points: { show: $("#lightPointsCheckbox").is(':checked') },
			shadowSize: 0
		}, {
			data: feedStartData,
			label: "Feed Start",
			// lines: {
			// 	show: true,
			// 	fill: false,
			// 	lineWidth: 2
			// },
			points: { show: true },
			shadowSize: 0
		}, {
			data: feedFinishData,
			label: "Feed Finish",
			// lines: {
			// 	show: true,
			// 	fill: false,
			// 	lineWidth: 2
			// },
			points: { show: true },
			shadowSize: 0
		}], {

			grid: {
				hoverable: true,
				clickable: true,
				tickColor: "rgba(255,255,255,0.05)",
				borderWidth: 0
			},
			legend: {
				show: false
			},
			colors: ["#EB3C00", "#2D89EF", "#FFC40D"],
			xaxis: {
				// ticks: 15,
				mode: "time",
				timeformat: "%H:%M",
				tickDecimals: 0,
				color: "rgba(255,255,255,0.8)"
			},
			yaxis: {
				// ticks: 5,
				tickDecimals: 0,
				tickFormatter: function(v) {
					return v + "&deg;C/dB/lx";
				},
				color: "rgba(255,255,255,0.8)"
			},
		});



		function showTooltip(x, y, contents) {
			$('<div id="tooltip">' + contents + '</div>').css({
				position: 'absolute',
				display: 'none',
				top: y + 5,
				left: x - 75,
				border: '1px solid #fdd',
				padding: '2px',
				'background-color': '#dfeffc',
				opacity: 0.80
			}).appendTo("body").fadeIn(200);
		}

		var previousPoint = null;
		$("#liveAll").bind("plothover", function(event, pos, item) {
			$("#x").text(pos.x.toFixed(2));
			$("#y").text(pos.y.toFixed(2));

			if (item) {
				if (previousPoint != item.dataIndex) {
					previousPoint = item.dataIndex;

					$("#tooltip").remove();
					var x = item.datapoint[0].toFixed(2),
						y = item.datapoint[1].toFixed(2);

					if (item.series.label === "Room Temperature") {
						showTooltip(item.pageX, item.pageY,
							item.series.label + " at " + ("0" + new Date(x * 1).getHours()).substr(-2) + ":" + ("0" + new Date(x * 1).getMinutes()).substr(-2) + ":" + ("0" + new Date(x * 1).getSeconds()).substr(-2) + " is " + y + "&deg;C");
					} else if (item.series.label === "Room Volume") {
						showTooltip(item.pageX, item.pageY,
							item.series.label + " at " + ("0" + new Date(x * 1).getHours()).substr(-2) + ":" + ("0" + new Date(x * 1).getMinutes()).substr(-2) + ":" + ("0" + new Date(x * 1).getSeconds()).substr(-2) + " is " + y + "dB");
					} else if (item.series.label === "Room Light") {
						showTooltip(item.pageX, item.pageY,
							item.series.label + " at " + ("0" + new Date(x * 1).getHours()).substr(-2) + ":" + ("0" + new Date(x * 1).getMinutes()).substr(-2) + ":" + ("0" + new Date(x * 1).getSeconds()).substr(-2) + " is " + y + "lx");
					} else if (item.series.label === "Feed Start") {
						showTooltip(item.pageX, item.pageY,
							item.series.label + "ed at " + ("0" + new Date(x * 1).getHours()).substr(-2) + ":" + ("0" + new Date(x * 1).getMinutes()).substr(-2) + ":" + ("0" + new Date(x * 1).getSeconds()).substr(-2));
					} else if (item.series.label === "Feed Finish") {
						showTooltip(item.pageX, item.pageY,
							item.series.label + "ed at " + ("0" + new Date(x * 1).getHours()).substr(-2) + ":" + ("0" + new Date(x * 1).getMinutes()).substr(-2) + ":" + ("0" + new Date(x * 1).getSeconds()).substr(-2));
					}



				}
			} else {
				$("#tooltip").remove();
				previousPoint = null;
			}
		});

	}


}

function sparkline_charts() {

	//generate random number for charts
	randNum = function() {
		//return Math.floor(Math.random()*101);
		return (Math.floor(Math.random() * (1 + 40 - 20))) + 20;
	}

	var chartColours = ['#ffffff', '#ffffff', '#ffffff', '#ffffff', '#ffffff', '#ffffff', '#ffffff'];

	//sparklines (making loop with random data for all 7 sparkline)
	i = 1;
	for (i = 1; i < 9; i++) {
		var data = [
			[1, 3 + randNum()],
			[2, 5 + randNum()],
			[3, 8 + randNum()],
			[4, 11 + randNum()],
			[5, 14 + randNum()],
			[6, 17 + randNum()],
			[7, 20 + randNum()],
			[8, 15 + randNum()],
			[9, 18 + randNum()],
			[10, 22 + randNum()]
		];
		placeholder = '.sparkLineStats' + i;

		if (retina()) {

			$(placeholder).sparkline(data, {
				width: 160, //Width of the chart - Defaults to 'auto' - May be any valid css width - 1.5em, 20px, etc (using a number without a unit specifier won't do what you want) - This option does nothing for bar and tristate chars (see barWidth)
				height: 80, //Height of the chart - Defaults to 'auto' (line height of the containing tag)
				lineColor: '#ffffff', //Used by line and discrete charts to specify the colour of the line drawn as a CSS values string
				fillColor: 'rgba(255,255,255,0.2)', //Specify the colour used to fill the area under the graph as a CSS value. Set to false to disable fill
				spotColor: '#ffffff', //The CSS colour of the final value marker. Set to false or an empty string to hide it
				maxSpotColor: '#ffffff', //The CSS colour of the marker displayed for the maximum value. Set to false or an empty string to hide it
				minSpotColor: '#ffffff', //The CSS colour of the marker displayed for the mimum value. Set to false or an empty string to hide it
				spotRadius: 2, //Radius of all spot markers, In pixels (default: 1.5) - Integer
				lineWidth: 1 //In pixels (default: 1) - Integer
			});

			$(placeholder).css('zoom', 0.5);

		} else {

			if ($.browser.msie && parseInt($.browser.version, 10) === 8) {

				$(placeholder).sparkline(data, {
					width: 80, //Width of the chart - Defaults to 'auto' - May be any valid css width - 1.5em, 20px, etc (using a number without a unit specifier won't do what you want) - This option does nothing for bar and tristate chars (see barWidth)
					height: 40, //Height of the chart - Defaults to 'auto' (line height of the containing tag)
					lineColor: '#ffffff', //Used by line and discrete charts to specify the colour of the line drawn as a CSS values string
					fillColor: '#ffffff', //Specify the colour used to fill the area under the graph as a CSS value. Set to false to disable fill
					spotColor: '#ffffff', //The CSS colour of the final value marker. Set to false or an empty string to hide it
					maxSpotColor: '#ffffff', //The CSS colour of the marker displayed for the maximum value. Set to false or an empty string to hide it
					minSpotColor: '#ffffff', //The CSS colour of the marker displayed for the mimum value. Set to false or an empty string to hide it
					spotRadius: 2, //Radius of all spot markers, In pixels (default: 1.5) - Integer
					lineWidth: 1 //In pixels (default: 1) - Integer
				});

			} else {

				$(placeholder).sparkline(data, {
					width: 80, //Width of the chart - Defaults to 'auto' - May be any valid css width - 1.5em, 20px, etc (using a number without a unit specifier won't do what you want) - This option does nothing for bar and tristate chars (see barWidth)
					height: 40, //Height of the chart - Defaults to 'auto' (line height of the containing tag)
					lineColor: '#ffffff', //Used by line and discrete charts to specify the colour of the line drawn as a CSS values string
					fillColor: 'rgba(255,255,255,0.2)', //Specify the colour used to fill the area under the graph as a CSS value. Set to false to disable fill
					spotColor: '#ffffff', //The CSS colour of the final value marker. Set to false or an empty string to hide it
					maxSpotColor: '#ffffff', //The CSS colour of the marker displayed for the maximum value. Set to false or an empty string to hide it
					minSpotColor: '#ffffff', //The CSS colour of the marker displayed for the mimum value. Set to false or an empty string to hide it
					spotRadius: 2, //Radius of all spot markers, In pixels (default: 1.5) - Integer
					lineWidth: 1 //In pixels (default: 1) - Integer
				});

			}

		}

	}

	if ($(".boxchart")) {

		if (retina()) {

			$(".boxchart").sparkline('html', {
				type: 'bar',
				height: '120', // Double pixel number for retina display
				barWidth: '8', // Double pixel number for retina display
				barSpacing: '2', // Double pixel number for retina display
				barColor: '#ffffff',
				negBarColor: '#eeeeee'
			});

			$(".boxchart").css('zoom', 0.5);

		} else {

			$(".boxchart").sparkline('html', {
				type: 'bar',
				height: '60',
				barWidth: '4',
				barSpacing: '1',
				barColor: '#ffffff',
				negBarColor: '#eeeeee'
			});

		}

	}

}

function retina() {

	retinaMode = (window.devicePixelRatio > 1);

	return retinaMode;

}